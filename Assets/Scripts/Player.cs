using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;

public class Player : MonoBehaviour
{
    [SerializeField]
    private Bullet _bulletPrefab;
    [SerializeField]
    private Laser _laserPrefab;

    private Camera _camera;
    private float _rotationSpeed = 7.0f;
    private Rigidbody2D _rigidbody2D;
    private Collider2D _collider2D;
    private BulletFactory _bulletFactory;
    private IObserver<Unit> _onGameOver;
    private IObserver<Vector3> _onDestroyed;

    public ReactiveProperty<int> NumberLaserSharges = new ReactiveProperty<int>(10);
    public ReactiveProperty<Vector2> Position = new ReactiveProperty<Vector2>();
    public ReactiveProperty<double> Rotation = new ReactiveProperty<double>();
    public ReactiveProperty<double> InstantSpeed = new ReactiveProperty<double>();
    public ReactiveProperty<float> SecondsPassed = new ReactiveProperty<float>();

    [Inject]
    private void Construct(
        Camera camera,        
        BulletFactory bulletFactory, 
        [Inject (Id = ZenjectID.ON_GAME_OVER_EVENT)] IObserver<Unit> OnGameOver,
        [Inject(Id = ZenjectID.ON_DESTROYED_EVENT)] IObserver<Vector3> OnDestroyed) 
    {
        _camera = camera;
        _bulletFactory = bulletFactory;
        _onGameOver = OnGameOver;
        _onDestroyed = OnDestroyed;
    }

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _collider2D = GetComponent<Collider2D>();
    }

    private void Start()
    {
        UniRxUpdate();
        UniRxFixedUpdate();
        UniRxOnCollisionEnter2D(_onGameOver, _onDestroyed);
    }

    private void UniRxUpdate()
    {
        Observable.EveryUpdate().Subscribe(value =>
        {
            if (Input.GetButtonDown("Fire1"))
            {
                BulletShoot();
            }
            else if (Input.GetButtonDown("Fire2"))
            {
                LaserShoot();
            }
            PlayerPosition();
            RollbackTimer(Time.deltaTime);
        }).AddTo(this);
    }

    private void UniRxFixedUpdate()
    {
        Observable.EveryFixedUpdate().Subscribe(value =>
        {
            InstantSpeed.Value = Mathf.Abs(Input.GetAxis("Vertical"));

            if (Input.GetAxis("Vertical") != 0)
            {
                _rigidbody2D.AddForce(transform.up * Input.GetAxis("Vertical"));
            }

            if (Input.GetAxis("Horizontal") != 0)
            {
                _rigidbody2D.AddTorque(Input.GetAxis("Horizontal") * _rotationSpeed * Time.deltaTime * -1);
            }

            Vector3 point = _camera.WorldToViewportPoint(transform.position);
            if (point.y < 0f || point.y > 1f)
            {
                transform.position = new Vector2(transform.position.x, transform.position.y * (-1));

            }
            else if (point.x < 0f || point.x > 1f)
            {
                transform.position = new Vector2(transform.position.x * (-1), transform.position.y);

            }
        }).AddTo(this);
    }        

    private void BulletShoot()
    {
        var bullet = _bulletFactory.GetNewInstance(transform.position, transform.rotation);
        bullet.MoveBullet(transform.up);
    }

    private void LaserShoot()
    {
        if (NumberLaserSharges.Value > 0)
        {
            Laser laser = Instantiate(_laserPrefab, transform.position, transform.rotation);
            MadeShoot();
            laser.DestroyLaser();
        }
    }

    private void MadeShoot()
    {
        NumberLaserSharges.Value--;
        if (NumberLaserSharges.Value == 9)
        {
            SecondsPassed.Value = 10;
        }
    }

    public void RollbackTimer(float deltaTime)
    {
        if (NumberLaserSharges.Value == 10)
            return;

        SecondsPassed.Value -= deltaTime;

        if (Math.Round(SecondsPassed.Value) <= 0f)
        {
            AddLaserShoot();
            SecondsPassed.Value = 10;
        }
    }

    private void AddLaserShoot()
    {
        NumberLaserSharges.Value++;
    }

    private void UniRxOnCollisionEnter2D(IObserver<Unit> OnGameOver, IObserver<Vector3> OnDestroyed)
    {
        _collider2D.OnCollisionEnter2DAsObservable()
             .Select(collision => collision.collider.name)
              .Where(name => (name.Contains("Asteroid")) || (name.Contains("UFO")))
              .Subscribe(_ =>
              {
                  OnGameOver.OnNext(Unit.Default);
                  OnDestroyed.OnNext(transform.position);
                  Destroy(gameObject);
              }).AddTo(this);
    }   

    private void PlayerPosition()
    {
        Position.Value = transform.position;
        Rotation.Value = Math.Round(transform.localEulerAngles.z, 1);
    }
}
