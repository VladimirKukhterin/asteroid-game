using UnityEngine;
using Zenject;

public class GenericFactory<T> : MonoBehaviour where T : MonoBehaviour
{
    [SerializeField]
    private T _prefab;

    private DiContainer _container;

    [Inject]
    private void Construct (DiContainer container)
    {
        _container = container;
    }
    
    public T GetNewInstance(Vector3 spawnPoint, Quaternion rotation)
    {
        T gameObject = Instantiate(_prefab, spawnPoint, rotation);
        _container.InjectGameObject(gameObject.gameObject);
        return gameObject;
    }
}
