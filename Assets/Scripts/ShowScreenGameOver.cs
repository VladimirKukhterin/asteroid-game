using System;
using UniRx;
using UnityEngine;
using Zenject;

public class ShowScreenGameOver : MonoBehaviour
{
    [SerializeField]
    private GameObject _gameOverGO;
    [SerializeField]
    private GameObject _shipInformationGO;

    private GameOver _gameOver;   

    [Inject]
    private void Construct(       
        GameOver gameOver,
        [Inject(Id = ZenjectID.ON_GAME_OVER_EVENT)] IObservable<Unit> onGameOver)
    {
        _gameOver = gameOver;       
        onGameOver.Subscribe(_ => ShowGameOverScreen());
    }

    public void ShowGameOverScreen()
    {
        _gameOver.ShowFinalScore();
        _shipInformationGO.SetActive(false);
        _gameOverGO.SetActive(true);        
    }
}
