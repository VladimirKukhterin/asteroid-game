using UnityEngine;

public class Laser : MonoBehaviour
{
    private float _lifeTime = 0.2f;
    public int Rotation { get; private set; }

    public void DestroyLaser()
    {
        Destroy(gameObject, _lifeTime);
    }
}
