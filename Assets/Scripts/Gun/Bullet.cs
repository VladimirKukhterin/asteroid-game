using UnityEngine;
using UniRx;
using UniRx.Triggers;
using Zenject;

public class Bullet : MonoBehaviour
{
    private Camera _camera;
    private float _speed = 500.0f;
    private float _lifeTime = 15.0f;
    private Rigidbody2D _rigidbody2D;
    private Collider2D _collider2D;

    [Inject]
    private void Construct(Camera camera)
    {
        _camera = camera;
    }

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _collider2D = GetComponent<Collider2D>();      
    }

    private void Start()
    {
        UniRxUpdate();
        UniRxOnCollisionEnter2D();
    }

    private void UniRxUpdate()
    {
        Observable.EveryUpdate()
           .Where(value => IsOutOfBounds())
           .Subscribe(_ => Destroy(gameObject))
           .AddTo(this);
    }

    private bool IsOutOfBounds()
    {
        Vector3 point = _camera.WorldToViewportPoint(transform.position);
        return point.y < 0f || point.y > 1f || point.x > 1f || point.x < 0f;       
    }

    public void MoveBullet(Vector2 vector)
    {
        _rigidbody2D.AddForce(vector * _speed);
        Destroy(gameObject, _lifeTime);
    }

    private void UniRxOnCollisionEnter2D()
    {
        _collider2D.OnCollisionEnter2DAsObservable()
            .Subscribe(_ => Destroy(gameObject)).AddTo(this);
    }
}
