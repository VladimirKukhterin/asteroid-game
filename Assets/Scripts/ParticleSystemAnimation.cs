using System;
using UniRx;
using UnityEngine;
using Zenject;

public class ParticleSystemAnimation : MonoBehaviour
{    
    private ParticleSystem _explosion;    

    [Inject]
    private void Construct([Inject(Id = ZenjectID.ON_DESTROYED_EVENT)] IObservable<Vector3> OnDestroyed)
    {       
        OnDestroyed.Subscribe(position => DestroyObject(position));
    }

    private void Awake()
    {
        _explosion = GetComponent<ParticleSystem>();
    }

    public void DestroyObject(Vector3 obj)
    {
        _explosion.transform.position = obj;
        _explosion.Play();
    }
}
