using UnityEngine;
using Zenject;

public class SpawnerEnemy : MonoBehaviour
{   
    private AsteroidFactory _asteroidFactory;
    private UFOFactory _ufoFactory;
    private float _trajectoryVariance = 15f;
    private float _spawnRate = 2f;
    private float _spawnDistance = 15f;
    private int _spawnAmout = 1;

    [Inject]
    private void Construct(AsteroidFactory asteroidFactory, UFOFactory ufoFactory)
    {
        _asteroidFactory = asteroidFactory;
        _ufoFactory = ufoFactory;
    }

    private void Start()
    {
        InvokeRepeating(nameof(Spawn), _spawnRate, _spawnAmout);
    }

    private void Spawn()
    {
        Vector3 spawnDirector = Random.insideUnitCircle.normalized * _spawnDistance;
        Vector3 spawnPoint = transform.position + spawnDirector;
        float variance = Random.Range(-_trajectoryVariance, _trajectoryVariance);
        Quaternion rotation = Quaternion.AngleAxis(variance, Vector3.forward);

        int randomObject = Random.Range(1, 3);
        switch (randomObject)
        {
            case 1:
                SpawnAsteroid(spawnDirector, spawnPoint, rotation);
                break;
            case 2:
                SpawnUFO(spawnDirector, spawnPoint, rotation);
                break;
            default:
                SpawnAsteroid(spawnDirector, spawnPoint, rotation);
                break;
        }
    }

    private void SpawnAsteroid(Vector3 spawnDirector, Vector3 spawnPoint, Quaternion rotation)
    {
        var asteroid = _asteroidFactory.GetNewInstance(spawnPoint, rotation);
        asteroid.Size = Random.Range(asteroid.MinSize, asteroid.MaxSize);
        asteroid.SetTrajectory(rotation * -spawnDirector);
    }

    private void SpawnUFO(Vector3 spawnDirector, Vector3 spawnPoint, Quaternion rotation)
    {
        var ufo = _ufoFactory.GetNewInstance(spawnPoint, rotation);
        ufo.SetTrajectory(rotation * -spawnDirector);
    }
}
