using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;


public class Asteroid : MonoBehaviour
{
    [SerializeField]
    private Sprite[] _sprites;

    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D _rigidbody2D;
    private float _speed = 5f;
    private float _lifeTime = 30.0f;
    private Collider2D _collider2D;
    private ScoreController _scoreController;
    private AsteroidFactory _asteroidFactory;
    private System.IObserver<Vector3> _onDestroyed;

    public float Size = 1f;
    public float MinSize = 0.5f;
    public float MaxSize = 1.5f;

    [Inject]
    private void Construct(
        ScoreController scoreController,
        AsteroidFactory asteroidFactory,
        [Inject(Id = ZenjectID.ON_DESTROYED_EVENT)] System.IObserver<Vector3> OnDestroyed)
    {
        _scoreController = scoreController;
        _asteroidFactory = asteroidFactory;
        _onDestroyed = OnDestroyed;
    }

    private void Awake()
    {
        Size = Random.Range(MinSize, MaxSize);
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _collider2D = GetComponent<Collider2D>();
    }

    private void Start()
    {
        _spriteRenderer.sprite = _sprites[Random.Range(0, _sprites.Length)];
        transform.eulerAngles = new Vector3(0f, 0f, Random.value * 360f);
        transform.localScale = Vector3.one * Size;
        _rigidbody2D.mass = Size;
        UniRxOnCollisionEnter2D();
        UniRxOnTriggerEnter2D();
    }

    public void SetTrajectory(Vector2 direction)
    {
        _rigidbody2D.AddForce(direction * _speed);
        Destroy(gameObject, _lifeTime);
    }

    private void UniRxOnCollisionEnter2D()
    {
        _collider2D.OnCollisionEnter2DAsObservable()
            .Where(collision => collision.collider.name.Contains("Bullet"))
            .Subscribe(_ =>
            {
                if ((Size * 0.5f) >= MinSize)
                {
                    CreateSplit();
                    CreateSplit();
                }
                Destroyed();
            }).AddTo(this);
    }

    private void UniRxOnTriggerEnter2D()
    {
        _collider2D.OnTriggerEnter2DAsObservable()
            .Where(collision => collision.gameObject.name.Contains("Laser"))
            .Subscribe(_ =>
            {
                Destroyed();
            }).AddTo(this);
    }

    private void CreateSplit()
    {
        Vector2 position = transform.position;
        position += Random.insideUnitCircle * 0.5f;

        var halfAsteroid = _asteroidFactory.GetNewInstance(position, transform.rotation);        
        halfAsteroid.Size = Size * 0.5f;
        halfAsteroid.SetTrajectory(Random.insideUnitCircle.normalized * _speed);
    }    

    private void Destroyed()
    {
        AsteroidDestroyed();
        _onDestroyed.OnNext(transform.position);
        Destroy(gameObject);
    }

    public void AsteroidDestroyed()
    {
        if (this.Size < 0.75f)
        {
            _scoreController.AddScore(100);
        }
        else if (this.Size < 1f)
        {
            _scoreController.AddScore(50);
        }
        else
        {
            _scoreController.AddScore(25);
        }
    }
}
