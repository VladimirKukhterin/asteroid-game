using UnityEngine;
using UniRx;
using UniRx.Triggers;
using Zenject;
using System;

public class UFO : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;
    private Collider2D _collider2D;
    private float _speed = 5f;
    private float _lifeTime = 30.0f;
    private IObserver<Vector3> _onDestroyed;
    private ScoreController _scoreController;

    [Inject]
    private void Construct(
        ScoreController scoreController,
        [Inject(Id = ZenjectID.ON_DESTROYED_EVENT)] IObserver<Vector3> OnDestroyed)
    {
        _scoreController = scoreController;
        _onDestroyed = OnDestroyed;
    }

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _collider2D = GetComponent<Collider2D>();
        UniRxOnCollisionEnter2D();
        UniRxOnTriggerEnter2D();
    }

    public void SetTrajectory(Vector2 direction)
    {
        _rigidbody2D.AddForce(direction * _speed);
        Destroy(gameObject, _lifeTime);
    }

    private void UniRxOnCollisionEnter2D()
    {
        _collider2D.OnCollisionEnter2DAsObservable()
            .Where(collision => collision.collider.name.Contains("Bullet"))
            .Subscribe(_ => Destroyed()).AddTo(this);
    }

    private void UniRxOnTriggerEnter2D()
    {
        _collider2D.OnTriggerEnter2DAsObservable()
            .Where(collision => collision.gameObject.name.Contains("Laser"))
            .Subscribe(_ => Destroyed()).AddTo(this);
    }

    private void Destroyed()
    {
        _onDestroyed.OnNext(transform.position);
        _scoreController.AddScore(20);
        Destroy(gameObject);
    }
}
