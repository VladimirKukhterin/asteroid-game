using System;
using TMPro;
using UnityEngine;
using UniRx;
using Zenject;

public class ShipInformation : MonoBehaviour
{          
    [SerializeField]
    private TextMeshProUGUI _coordinatesText;
    [SerializeField]
    private TextMeshProUGUI _angleRotationText;
    [SerializeField]
    private TextMeshProUGUI _instantSpeedText;
    [SerializeField]
    private TextMeshProUGUI _numberLaserShargesText;
    [SerializeField]
    private TextMeshProUGUI _laserRollbackTime;
    [SerializeField]
    private TextMeshProUGUI _scoreText;

    private Player _player;
    private ScoreController _scoreController;

    [Inject]
    private void Construct(ScoreController scoreController, Player player)
    {
        _scoreController = scoreController;
        _player = player;
    }

    private void Start()
    {
        ShowPositionPlayer();
        ShowRotationPlayer();
        ShowInstantSpeed();
        ShowNumberLaserSharges();
        ShowlaserRollbackTime();
        ShowScore();
    }   

    private void ShowPositionPlayer()
    {
        _player.Position.Subscribe(value =>
        {
            _coordinatesText.text = $"����������: {_player.Position}";
        }).AddTo(this);
    }

    private void ShowRotationPlayer()
    {
        _player.Rotation.Subscribe(value =>
        {
            _angleRotationText.text = $"���� ��������: {value}�";
        }).AddTo(this);
    }

    private void ShowInstantSpeed()
    {
        _player.InstantSpeed.Subscribe(value =>
        {
            _instantSpeedText.text = $"���������� ��������: { Math.Round(value, 2)}";
        }).AddTo(this);
    }

    private void ShowNumberLaserSharges()
    {
        _player.NumberLaserSharges.Subscribe(value =>
        {
            _numberLaserShargesText.text = $"����� ������� ������: {value}/10";
        }).AddTo(this);
    }

    private void ShowlaserRollbackTime()
    {
        _player.SecondsPassed.Subscribe(value =>
        {
            if (_player.NumberLaserSharges.Value == 10)
            {
                _laserRollbackTime.text = $"����� ������ ������: ����� �����";
            }
            else
            {
                _laserRollbackTime.text = $"����� ������ ������: {Math.Round(value)}";
            }
        }).AddTo(this);
    }

    private void ShowScore()
    {
        _scoreController.Score.Subscribe(value =>
        {
            _scoreText.text = $"����: {value}";
        }).AddTo(this);
    }
}
