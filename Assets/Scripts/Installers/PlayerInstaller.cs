using UnityEngine;
using Zenject;

public class PlayerInstaller : MonoInstaller
{    
    [SerializeField]
    private GameObject _playerPrefab; 
    [SerializeField]
    private Transform _startPoint;
    
    public override void InstallBindings()
    {                
        Player _player = Container
            .InstantiatePrefabForComponent<Player>(_playerPrefab, _startPoint.position, Quaternion.identity, null);

        Container
            .Bind<Player>()
            .FromInstance(_player)
            .AsSingle()
            .NonLazy();
    }
}