using System;
using UniRx;
using UnityEngine;
using Zenject;

public class GameManagerInstaller : MonoInstaller
{   
    [SerializeField]
    private GameOver _gameOver;
    [SerializeField]
    private Camera _camera;
    [SerializeField]
    private ScoreController _scoreController;

    public override void InstallBindings()
    {
        Container
          .Bind<Camera>()
          .FromInstance(_camera)
          .AsSingle();       
               
        Container
            .Bind<ScoreController>()
            .FromInstance(_scoreController)
            .AsSingle();

        Container
            .Bind<GameOver>()
            .FromInstance(_gameOver)
            .AsSingle();

        var OnGameOver = new Subject<Unit>();
        Container.Bind<IObserver<Unit>>()
             .WithId(ZenjectID.ON_GAME_OVER_EVENT)
             .FromInstance(OnGameOver)
             .NonLazy();
        Container.Bind<IObservable<Unit>>()
             .WithId(ZenjectID.ON_GAME_OVER_EVENT)
             .FromInstance(OnGameOver)
             .NonLazy();       
    }
}