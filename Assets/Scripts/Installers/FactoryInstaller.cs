using UnityEngine;
using Zenject;

public class FactoryInstaller : MonoInstaller
{ 
    [SerializeField]
    private AsteroidFactory _asteroidFactory;
    [SerializeField]
    private UFOFactory _ufoFactory;
    [SerializeField]
    private BulletFactory _bulletFactory;
       
    public override void InstallBindings()
    {  
        Container
            .Bind<AsteroidFactory>()
            .FromComponentInNewPrefab(_asteroidFactory)
            .AsSingle();

        Container
            .Bind<UFOFactory>()
            .FromComponentInNewPrefab(_ufoFactory)
            .AsSingle();

        Container
            .Bind<BulletFactory>()
            .FromComponentInNewPrefab(_bulletFactory)
            .AsSingle();
    }
}