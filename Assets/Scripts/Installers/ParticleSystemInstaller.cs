using System;
using UniRx;
using UnityEngine;
using Zenject;

public class ParticleSystemInstaller : MonoInstaller
{
    [SerializeField]
    private ParticleSystemAnimation _particleSystemAnimation;

    public override void InstallBindings()
    {
        Container
           .Bind<ParticleSystemAnimation>()
           .FromComponentInNewPrefab(_particleSystemAnimation)
           .AsSingle()
           .NonLazy();

        var OnDestroyed = new Subject<Vector3>();
        Container.Bind<IObserver<Vector3>>()
             .WithId(ZenjectID.ON_DESTROYED_EVENT)
             .FromInstance(OnDestroyed)
             .NonLazy();
        Container.Bind<IObservable<Vector3>>()
             .WithId(ZenjectID.ON_DESTROYED_EVENT)
             .FromInstance(OnDestroyed)
             .NonLazy();
    }
}