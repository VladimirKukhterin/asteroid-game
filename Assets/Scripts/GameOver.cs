using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UniRx;
using Zenject;

public class GameOver : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _scoreText;

    private float secondsPassed = 0;
    private ScoreController _scoreController;

    [Inject]
    private void Construct(ScoreController scoreController)
    {      
        _scoreController = scoreController;        
    }

    private void Start()
    {
        UniRxUpdate();
    }

    private void UniRxUpdate()
    {
        Observable.EveryUpdate().Subscribe(_ =>
        {
            secondsPassed += Time.deltaTime;

            if (Math.Round(secondsPassed) > 1f)
            {
                if (Input.anyKeyDown)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 0);
                }               
            }
        }).AddTo(this);
    }

    public void ShowFinalScore()
    {
        _scoreText.text = $"��� ����: {_scoreController.Score.Value}";
    }
}
