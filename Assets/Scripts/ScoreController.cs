using UniRx;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    public ReactiveProperty<int> Score = new ReactiveProperty<int>(0);

    private void OnEnable()
    {
        Score.Value = 0;
    }   

    public void AddScore(int score)
    {       
        Score.Value += score;
    }
}
